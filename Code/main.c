
#include "libchessgui.h"

//Palette of colours for the chess board squares
SDL_Color lightBrown = {181, 136, 99, SDL_ALPHA_OPAQUE};
SDL_Color lightWood = {240, 217, 181, SDL_ALPHA_OPAQUE};
SDL_Color greenish = {155, 199, 0, 0.41};
#define DARK  lightBrown
#define LIGHT lightWood
#define TRANSFORM_COL greenish

//Point of view on witch the board will be rendered (WHITE or BLACK). Can be changed at will during any moment of the game
Uint8 pointOfView = BLACK;

int main(void){

  //Initialize SDL resources
  SDL_Window* win; SDL_Renderer* rend; SDL_Surface* piecesSurf; SDL_Texture* piecesTex;
  if(initSDL_resources(&win, &rend, &piecesSurf, &piecesTex)) return 1;

  //Iniialize the SDL sound delSDL_resources
  Wav_file move_wav;
  Wav_file capture_wav;
  if(initSDL_Sound(&move_wav, &capture_wav)) return 1;


  //Initialize the needed variables for the game
  Uint8 Board[64] = {0}; //Memory representation of the chess board. Initialized to 0
  ChessMove lastMove = {-1,-1, NONE}; //Last move trayed to play (legal or ilegal)
  ChessMove validatedLastMove = {-1,-1, NONE}; //Last legal move played
  ChessGame game;

  char StartFEN[] = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"; //Starting position
  ParseFenToBoard(StartFEN, Board, sizeof(StartFEN), &game); //Put the FEN position on the Board variable

  //Animation loop
  int squareSize = (int) (WINDOW_HEIGHT/8);
  int w, h; //Current width and height of the window
  Uint8 playSound = 0; //Flag used to play a capture/move sound
  Uint8 pieceMoved = 0; //Flag to see when a piece is moved, and check if its legal
  Uint8 close_requested = 0;     // set to 1 when window close button is pressed

  while (!close_requested){

      //Process events (user input)
      SDL_Event event;
      while (SDL_PollEvent(&event)){
        switch(event.type){
          case(SDL_QUIT): //If the window its clossed
            close_requested = 1; //Stop the loop
            break;
          case(SDL_MOUSEBUTTONDOWN):  //If the left mouse button is pressed and the mouse is inside the board
            if(event.button.button == SDL_BUTTON_LEFT && event.button.x<h && event.button.y<h){
              lastMove.fromSquare = (Uint8) ((event.button.x/squareSize)+8*(event.button.y/squareSize)); //Find the square where the cursor is
              if(pointOfView==BLACK){
                //Aply a 180ª transform, centered on the midle of the board
                lastMove.fromSquare = -lastMove.fromSquare+63;
              }
              if(Board[lastMove.fromSquare]){ //If there's a piece on the pressed square
                lastMove.pieceMoved = Board[lastMove.fromSquare]; //"Attach" the piece to the mouse
                Board[lastMove.fromSquare] = 0; //Remove the piece from the board
              }
            }
            break;
          case(SDL_MOUSEBUTTONUP):  //If the left button is released and the mouse is inside the board
            if(event.button.button == SDL_BUTTON_LEFT && event.button.x<h && event.button.y<h){
              lastMove.toSquare = (Uint8) ((event.button.x/squareSize)+8*(event.button.y/squareSize)); //Find the square where the cursor is
              if(pointOfView==BLACK){
                //Aply a 180ª transform, centered on the midle of the board
                lastMove.toSquare = -lastMove.toSquare+63;
              }

              pieceMoved = 1; //Flag to check if a move is legal or not
            }else{                 //If the mouse is outside the board when released
              Board[lastMove.fromSquare] = lastMove.pieceMoved; //Put the piece on its original square
              lastMove.pieceMoved = NONE; //Deattach the piece from the mouse
            }
            break;
          default: break;
        }
      }

//START OF THE GAME LOGIC
      if(pieceMoved){
        pieceMoved = 0; //Reset the flag
        if(!isMoveIlegal(Board, &lastMove, &game)){ //If the move is legal
          validatedLastMove = lastMove;
          //Play the move/capture sound
          if((Board[lastMove.toSquare]) || ((validatedLastMove.toSquare == game.enPassantSquare) && ((validatedLastMove.pieceMoved&(~BOTH_COLOURS))==PAWN))){
            playSound = CAPTURE_SOUND;
          }else{
            playSound = MOVE_SOUND;
          }
          Board[lastMove.toSquare] = lastMove.pieceMoved; //Put the piece back to the board;
          lastMove.pieceMoved = NONE; //Deattach the piece from the mouse

          //Actualize game.variables
          if(game.nextToPlay == BLACK) game.fullMoveClock++; //After black move, increment the full move clock
          game.nextToPlay = game.nextToPlay==WHITE ? BLACK:WHITE; //Change whose turn is

        }else{ //If the move is ilegal:
          SDL_Log("You fool!\n"); //DEBUG
          Board[lastMove.fromSquare] = lastMove.pieceMoved; //Put the piece on its original square
          lastMove.pieceMoved = NONE; //Deattach the piece from the mouse
        }
      }
  //END OF THE GAME LOGIC

  //START OF THE DRAWING

      //Get current dimensions of the window before drawings
      SDL_GetWindowSize(win, &w, &h);
      squareSize = (int) (h/8); //Calculate the apropiate size of each square of the board

      SDL_SetRenderDrawColor(rend, 255, 255, 255, SDL_ALPHA_OPAQUE); //Set white as the drawing color
      SDL_RenderClear(rend); //Clear the window with the drawing color

      //Draw the chess board
      DrawBoard(rend, squareSize, 0, 0, &DARK, &LIGHT, &TRANSFORM_COL, validatedLastMove, pointOfView); //renderer, each square size, starting pos x and y, chosen colors for the board

      //Draw a board state on the screen board from WHITE's point of view
      DrawPieces(Board, squareSize, rend, piecesTex, pointOfView);
      //If there's a piece "attached" to the mouse
      if(lastMove.pieceMoved && event.button.x>0){ //And the mouse is on the board...
        //Draw it on the mouse coordinates
        SDL_Rect sprite = FindSpriteFromTex(lastMove.pieceMoved); //Chose the right sprite to draw
        SDL_Rect sprite_pos = {event.button.x-squareSize/2, event.button.y-squareSize/2, squareSize, squareSize}; //Find the coordinates where the sprite must be drawn
        SDL_RenderCopy(rend, piecesTex, &sprite, &sprite_pos); //Draw it!
      }
      //Switch the buffers
      SDL_RenderPresent(rend);
  //END OF THE DRAWING

  //Play a sound, if a move have been made
  switch(playSound){
    case MOVE_SOUND:
      ProduceSound(&move_wav);
      playSound = 0;
      break;
    case CAPTURE_SOUND:
      ProduceSound(&capture_wav);
      playSound = 0;
      break;
    case CHECK_MATE_SOUND:
        break;
    default: break;
  }
    }

    //Clear up resources before exiting
    delSDL_Soud(&move_wav, &capture_wav);
    delSDL_resources(&win, &rend, &piecesSurf, &piecesTex);
    return 0;
}
