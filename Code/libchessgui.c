 #include "libchessgui.h"

//Allocate/Deallocate resources
int initSDL_resources(SDL_Window** win, SDL_Renderer** rend, SDL_Surface** surface, SDL_Texture** tex){
  //Initializes SDL Graphical resources

  //Attempt to initialize graphics and timer system
  if (SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO) != 0){
      SDL_LogError(0, "error initializing SDL: %s\n", SDL_GetError());
      return 1;
  }

  Uint32 window_flags = SDL_WINDOW_RESIZABLE | SDL_WINDOW_MINIMIZED; //Make the window resizable and able to maximizeit
  *win = SDL_CreateWindow("Chess GUI", //Create the window
                                     SDL_WINDOWPOS_CENTERED,
                                     SDL_WINDOWPOS_CENTERED,
                                     WINDOW_WIDTH, WINDOW_HEIGHT, window_flags);
  if (!(*win)){ //If the window can't be created
      SDL_LogError(0, "error creating window: %s\n", SDL_GetError()); //get the error and quit
      SDL_Quit();
      return 1;
  }

  Uint32 render_flags = SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC; //USe hardware renderer and synchornized with the refresh rate
  *rend = SDL_CreateRenderer(*win, -1, render_flags); //Creatr a renderer for the window
  if(!(*rend)){ //If the renderer can't be created
    SDL_LogError(0, "Error creating renderer: %s\n", SDL_GetError()); //get the error and quit
    SDL_DestroyWindow(*win);
    SDL_Quit();
    return 1;
  }

  *surface =IMG_Load("resources/pieces.png"); //Create a surface (image in memory)
  if (!(*surface)){ //If the surface can't be created
    SDL_LogError(0, "Error creating a surface: %s\n", SDL_GetError()); //Show me the eror and quit
    SDL_DestroyRenderer(*rend);
    SDL_DestroyWindow(*win);
    SDL_Quit();
    return 1; //Return with error code
  }

  *tex = SDL_CreateTextureFromSurface(*rend, *surface); //Create a texture (image in hardware memory)
  if(!(*tex)){ //IF the texture can't be created
    SDL_LogError(0, "Error creating a texture: %s\n", SDL_GetError()); //Show the error and free resources
    SDL_FreeSurface(*surface);
    SDL_DestroyRenderer(*rend);
    SDL_DestroyWindow(*win);
    SDL_Quit();
    return 1;
  }
  return 0;
}
void delSDL_resources(SDL_Window** win, SDL_Renderer** rend, SDL_Surface** surface, SDL_Texture** tex){
  //Destroy all SDL resources allocated by initSDL_resources function
  SDL_DestroyTexture(*tex);
  SDL_FreeSurface(*surface);
  SDL_DestroyRenderer(*rend);
  SDL_DestroyWindow(*win);
  SDL_Quit();
}

//Sound stuff
int initSDL_Sound(Wav_file* move_wav, Wav_file* capture_wav){
  //Inits he sounds that will be played during the game

  //Load the Move.wav file
  if (SDL_LoadWAV("resources/Move.wav", &(move_wav->wav_spec), &(move_wav->wav_buffer), &(move_wav->wav_length)) == NULL) {
      SDL_LogError(0, "Could not open Move.wav: %s\n", SDL_GetError());
      return 1;
  }
  //Load the Capture.wav file
  if (SDL_LoadWAV("resources/Capture.wav", &(capture_wav->wav_spec), &(capture_wav->wav_buffer), &(capture_wav->wav_length)) == NULL) {
      SDL_LogError(0, "Could not open Capture.wav: %s\n", SDL_GetError());
      return 1;
  }
  //Open a Audio Devic
  move_wav->m_device = SDL_OpenAudioDevice(NULL, 0, &(move_wav->wav_spec), NULL, SDL_AUDIO_ALLOW_ANY_CHANGE);
  if(!(move_wav->m_device)){
      SDL_LogError(0, "Could not open audio device: %s\n", SDL_GetError());
      return 1;
  }
  //Same device is used for the Capture sound and the Move sound.
  capture_wav->m_device = move_wav->m_device;
  return 0;
}
int ProduceSound(Wav_file* file_wav){
  if(SDL_QueueAudio(file_wav->m_device, file_wav->wav_buffer, file_wav->wav_length) != 0){
    SDL_LogError(0, "Could not open QueueAudio: %s\n", SDL_GetError());
    return 1;
  }
  SDL_PauseAudioDevice(file_wav->m_device, 0);
  return 0;
}
void delSDL_Soud(Wav_file* move_wav, Wav_file* capture_wav){
  //Destroy all SDL audio resources allocated by the initSDL_Sound function
  SDL_FreeWAV(move_wav->wav_buffer);
  SDL_FreeWAV(capture_wav->wav_buffer);
  SDL_CloseAudioDevice(move_wav->m_device);
}

//Graphical stuff
void DrawBoard(SDL_Renderer* renderer, Uint16 squareSize, Uint16 xPos,  Uint16 yPos, SDL_Color *Dark, SDL_Color *Light, SDL_Color* TransformCol, ChessMove lastMove, Uint8 pointOfView){
  //Draws a chess board using a renderer, squareSize is the size of each of the 64 squares. xPos and yPos the starting position for the drawings
  //Dark and White are the light ad dark square colors of the board
  SDL_Color* squareCol; //Pointer to the color of the board squares
  Uint8 file_t, rank_t;

  for(Uint8 file=0; file<8; file++){ //For each of the 64 squares
    for(Uint8 rank=0; rank<8; rank++){
      squareCol = (file+rank)%2 == 0 ? Light : Dark; //Check if the color should be Dark or Light

      if(pointOfView==BLACK){
        //Aply a 180ª transform, centered on the midle of the board
        file_t = -file+7;
        rank_t = -rank+7;
      }else{
        file_t = file;
        rank_t = rank;
      }
      //Mark the last move made with diferent colours
      if(lastMove.fromSquare==file_t+rank_t*8 || lastMove.toSquare==file_t+rank_t*8){
        SDL_SetRenderDrawColor(renderer, (squareCol->r+TransformCol->r)/2, (squareCol->g+TransformCol->g)/2, (squareCol->b+TransformCol->b)/2, (squareCol->a+TransformCol->a)/2); //Set the renderer to the right color
      }else{SDL_SetRenderDrawColor(renderer, squareCol->r, squareCol->g, squareCol->b, squareCol->a);} //Set the renderer to the right color

      SDL_Rect rect = {xPos+file*squareSize, yPos+rank*squareSize, squareSize, squareSize}; //Position the square in place
      SDL_RenderFillRect(renderer, &rect); //And draw it
    }
  }
}
SDL_Rect FindSpriteFromTex(Uint8 piece){
  //Just a big switch... Takes a Uint8 representing a piece, and returns an SDL_Rect with the coordinates to such sprite on the texture.
  SDL_Rect sprite;
  sprite.w = 334; //Each sprite has a w and h of 334
  sprite.h = 334;
  switch(piece){
    case(W_KING):
      sprite.x = 0;
      sprite.y = 0;
      break;
    case(W_QUEEN):
      sprite.x = 334;
      sprite.y = 0;
     break;
    case(W_BISHOP):
      sprite.x = 668;
      sprite.y = 0;
      break;
    case(W_KNIGHT):
      sprite.x = 1002;
      sprite.y = 0;
      break;
    case(W_ROOK):
      sprite.x = 1336;
      sprite.y = 0;
      break;
    case(W_PAWN):
      sprite.x = 1670;
      sprite.y = 0;
      break;

    case(B_KING):
      sprite.x = 0;
      sprite.y = 334;
      break;
    case(B_QUEEN):
      sprite.x = 334;
      sprite.y = 334;
      break;
    case(B_BISHOP):
      sprite.x = 668;
      sprite.y = 334;
      break;
    case(B_KNIGHT):
      sprite.x = 1002;
      sprite.y = 334;
      break;
    case(B_ROOK):
      sprite.x = 1336;
      sprite.y = 334;
      break;
    case(B_PAWN):
      sprite.x = 1670;
      sprite.y = 334;
      break;

    default:
    sprite.x = 0;
    sprite.y = 0;
    sprite.w = 0;
    sprite.h = 0;
    }
  return sprite;
}
void DrawPieces(Uint8* Board, int squareSize, SDL_Renderer* rend, SDL_Texture* tex, Uint8 pointOfView){
  //Takes the pointer to a board (64 elements, uint8 array) and draws each piece there is on it.
  //pointOfView draws pieces from the black perspective if = BLACK or to the WHITE perspective if it = WHITE
  Uint8 j_t, i_t;
  for (Uint8 i=0;i<8;i++){
    for(Uint8 j=0;j<8;j++){ //For each square of the board
      if(*Board != 0){ //If there's a piece on that square
        if(pointOfView==BLACK){
          //Aply a 180ª transform, centered on the midle of the board
          j_t = -j+7;
          i_t = -i+7;
        }else{
          j_t = j;
          i_t = i;
        }
        SDL_Rect sprite = FindSpriteFromTex(*Board); //Chose the right sprite to draw
        SDL_Rect sprite_pos = {j_t*squareSize, i_t*squareSize, squareSize, squareSize}; //Find the coordinates where the sprite must be drawn
        SDL_RenderCopy(rend, tex, &sprite, &sprite_pos); //Draw it!
      }
      Board++; //And go to the next square
    }
  }
}

//General game logic stuff
void ParseFenToBoard(char* fen, Uint8* Board, Uint8 fen_len, ChessGame* game){
  //Reads a position in FEN and translates it into a Board state
  //It asumes the FEN string is already checked and correctly written

  Uint8 fenField = 0; //What field are we in?
    //0 = Pieces Position
    //1 = Active color (who moves next? white or black)
    //2 = Castling privilegi (KQkq)
    //3 = En passant target square
    //4 = The number of halfmoves since the last capture or pawn advance, used for the fifty-move rule
    //5 = Fullmove number: The number of the full move. It starts at 1, and is incremented after Black's move.

  game->castleRights=0; //Make sure this variable is initialized to 0
  for(int charCount=0; charCount<fen_len; fen++){ //For each char of the FEN string. This For loop looks weird, but its OK. Sorry
    charCount++; //Increment the counter, go to the next char

    //If the final piece has been placed, go to the next field
    if(*fen == ' '){
      fenField++;
      continue;
    }

    switch(fenField){
      case 0: //Parse positions to the board
      //If a digit is found, skip that many squares
        if(isdigit(*fen)) Board += *fen - '0'; //cast from char to int
        switch(*fen){
        //If a piece is found, place that piece in place
        case('r'):
          *Board = B_ROOK;
          Board++;
          break;
        case('b'):
          *Board = B_BISHOP;
          Board++;
          break;
        case('n'):
          *Board = B_KNIGHT;
          Board++;
          break;
        case('p'):
          *Board = B_PAWN;
          Board++;
          break;
        case('k'):
          *Board = B_KING;
          Board++;
          break;
        case('q'):
          *Board = B_QUEEN;
          Board++;
          break;
        case('R'):
          *Board = W_ROOK;
          Board++;
          break;
        case('B'):
          *Board = W_BISHOP;
          Board++;
          break;
        case('N'):
          *Board = W_KNIGHT;
          Board++;
          break;
        case('P'):
          *Board = W_PAWN;
          Board++;
          break;
        case('K'):
          *Board = W_KING;
          Board++;
          break;
        case('Q'):
          *Board = W_QUEEN;
          Board++;
          break;
          default: break;}
      break;
      case 1: //Who plays next?
        if(*fen == 'w') {game->nextToPlay = WHITE;}
        else{game->nextToPlay = BLACK;}
      break;
      case 2:  //Translate the castle privilegi to a ChesGame Uint
      switch(*fen){
        case('K'): game->castleRights+=WHITE_CASTLE_KING; break;
        case('Q'): game->castleRights+=WHITE_CASTLE_QUEEN; break;
        case('k'): game->castleRights+=BLACK_CASTLE_KING; break;
        case('q'): game->castleRights+=BLACK_CASTLE_QUEEN; break;
        case('-'): game->castleRights=0; break;
        default: break;
      }
      break;
      case 3: //Find the target en passant square in the Board[] space
      if(*fen == '-'){ //Case if there isn't a enPassantSquare
        game->enPassantSquare = 255;
        continue;
      }else{
      Uint8 file = *fen-'a';
      Uint8 rank = 7-(*(++fen)-'1');
      game->enPassantSquare = file + 8*rank;
      charCount++; //skip one char, since we've processed 2 in this field
      }
      break;
      case 4: //Grab the halfmove clock
        game->halfMoveClock = *fen - '0'; //Put the first digit of the clock on the variable
        if(*(++fen) == ' '){ //If next char is a ' ', we are done
          --fen;
          break;
        }else{
          charCount++; //Skip one char on the for loop
          game->halfMoveClock = (game->halfMoveClock)*10 + (*fen-'0'); //Put the second digit on the clock
          if(*(++fen) == ' '){ //If next char is a ' ', we are done
            --fen;
            break;
        }else{
          charCount++; //Skip a second char on the for loop
          game->halfMoveClock = (game->halfMoveClock)*10 + (*fen-'0'); //Put the third digit on the clock
          if(*(++fen) == ' '){ //If next char is a ' ', we are done
            --fen;
            break;
          }
        }
      }
      break;
      case 5:  //Grab the fullmove clock
        game->fullMoveClock = *fen - '0'; //Put the first digit of the clock on the variable

        if(*(++fen) == '\0'){ //If next char is a ' ', we are done
          --fen;
          charCount++; //End of the for
          break;
        }else{
          charCount++; //Skip one char on the for loop
          game->fullMoveClock = (game->fullMoveClock)*10 + (*fen-'0'); //Put the second digit on the clock
          if(*(++fen) == '\0'){ //If next char is a ' ', we are done
            --fen;
            charCount++; //End of the for
            break;
        }else{
          charCount++; //Skip a second char on the for loop
          game->fullMoveClock = (game->fullMoveClock)*10 + (*fen-'0'); //Put the third digit on the clock
          if(*(++fen) == '\0'){ //If next char is a ' ', we are done
            --fen;
            charCount++; //End of the for
            break;
          }
        }
      }
      break;
      default: break;
    }
  }
}

//Check for move legality stuff
typedef struct{
  //Number of squares from the "fromSquare" an edge following each of the eight Queen-trajectories
  Uint8 Up:3;
  Uint8 Down:3;
  Uint8 Left:3;
  Uint8 Right:3;
  Uint8 UpLeft:3;
  Uint8 UpRight:3;
  Uint8 DownLeft:3;
  Uint8 DownRight:3;
}_edgeDistance;
Uint8 _get_direction(_edgeDistance* squares_to_edge, Uint8 dirNumber){
  //Used to acces _edgeDistance structures bu index, as if they were arrays.
  switch(dirNumber){
    case 0: return squares_to_edge->Up;
    case 1: return squares_to_edge->Down;
    case 2: return squares_to_edge->Left;
    case 3: return squares_to_edge->Right;
    case 4: return squares_to_edge->UpLeft;
    case 5: return squares_to_edge->UpRight;
    case 6: return squares_to_edge->DownLeft;
    case 7: return squares_to_edge->DownRight;
    default: return 0;
  }
}
Uint8 SlidingMovesLegality(Uint8 startDirection, Uint8 endDirection, Uint8* Board, ChessMove* lastMove){
  //Functio used to find if the QUEEN, ROOK, or BISHOP move could be legal
   //Check if a move could be legal or not according to the moving pattern of the Bishop, Rook and Queen.
   _edgeDistance squares_to_edge;
   squares_to_edge.Up = lastMove->fromSquare/8;
   squares_to_edge.Down = 7-lastMove->fromSquare/8;
   squares_to_edge.Left = lastMove->fromSquare%8;
   squares_to_edge.Right = 7-lastMove->fromSquare%8;
   squares_to_edge.UpLeft = fmin(squares_to_edge.Up, squares_to_edge.Left);
   squares_to_edge.UpRight = fmin(squares_to_edge.Up, squares_to_edge.Right);
   squares_to_edge.DownLeft = fmin(squares_to_edge.Down, squares_to_edge.Left);
   squares_to_edge.DownRight = fmin(squares_to_edge.Down, squares_to_edge.Right);


   Uint8 _directionIncrement[8] = {-8, 8, -1, 1, -9, -7, 7, 9}; //Increments to move from one square to the other on each direction
   Uint8 checking_square; //Squares the piece would walk through if it is moving in a direction according to its pattern
   Uint8 move_could_be_legal = 0;

   //This two fors check if a move could be LEGAL, not if it is ilegal
   for(Uint8 direction=startDirection; direction<endDirection; direction++){ //For each direction
     checking_square = lastMove->fromSquare;
     Uint8 squaresToEdge = _get_direction(&squares_to_edge, direction);
     for(Uint8 squares=0; squares<squaresToEdge; squares++){ //For each square of such direction
      checking_square+=_directionIncrement[direction]; //Move one step on such direction

      if(Board[checking_square]){  //If there's a piece on your way the move is ilegal
        squares=squaresToEdge; //Break the inner for loop, look at the next direction.
     }

      if(checking_square==lastMove->toSquare){ //If the move could be legal
        move_could_be_legal = 1;
        squares=squaresToEdge; //Break the inner for loop
        direction=endDirection; //Break the outer for loop
      }
     }
   }
   return move_could_be_legal;
 }
Uint8 isMoveIlegal(Uint8* Board, ChessMove* lastMove, ChessGame* game){
  //Check if a move is ilegal or not. Returns 0 for legal moves and 1 otherwise
  //It also changes the Board[] in order to reflect castling and en passant correctly

  int mov_dir = lastMove->toSquare-lastMove->fromSquare; //Moving direction of the piece
  int mov_color = (lastMove->pieceMoved) & (BOTH_COLOURS); //the color of the last moved piece (= BLACK or WHITE)

  if(!mov_dir) return 1; //If the piece hasn't move, the move is ilegal (what move?)
  if(mov_color != game->nextToPlay) return 1; //The piece moved is of the corresponding color? (Don't touch my pieces!)
  if(((Board[lastMove->toSquare]) & (BOTH_COLOURS)) == mov_color) return 1; //You can't capture your own pieces (Why would you? To avoid checkmate? hahaha)

  //Start by checking if the piece tried to move according to its pattern
  switch(lastMove->pieceMoved & ANY_PIECE){ //Black and white pieces move the same way
    case(KING):
      //Check for casteling here. If can castle break, else return 1

      //If the king hasn't move according to his pattern the move is ilegal, so return 1
      if(!(mov_dir==-9||mov_dir==-8||mov_dir==-7||mov_dir==-1||mov_dir==1||mov_dir==7||mov_dir==8||mov_dir==9)){
        return 1;
      }else{
      break;}
    case(QUEEN):
      if(!SlidingMovesLegality(0, 8, Board, lastMove)) return 1; //If the move could not be legal skip the rest of the function plis
      break; //Else keep checking. You lazy machine!

    case(ROOK):
      if(!SlidingMovesLegality(0, 4, Board, lastMove)) return 1; //If the move could not be legal skip the rest of the function plis
      break;

    case(BISHOP):
    if(!SlidingMovesLegality(4, 8, Board, lastMove)) return 1; //If the move could not be legal skip the rest of the function plis
    break; //Keep checking.

    case(KNIGHT):
      if(!(mov_dir==-17||mov_dir==-15||mov_dir==-10||mov_dir==-6||mov_dir==6||mov_dir==10||mov_dir==15||mov_dir==17)) return 1;
      break;
    case(PAWN):
      if(mov_color == BLACK) mov_dir = mov_dir*-1; //The black pawn moves backwards
      if((mov_dir==-8 || mov_dir==-16) && Board[lastMove->toSquare]) return 1; //Can't capture by going forward Mr. Pawn!

      if((lastMove->fromSquare/8==6 && mov_color == WHITE) || (lastMove->fromSquare/8==1 && mov_color == BLACK)){ //If the pawn is on the 2nd rank(hasn't move) (or the 7th rank for black pawns)
        if(!(mov_dir==-8||mov_dir==-16||((mov_dir==-7)&&(Board[lastMove->toSquare]))||((mov_dir==-9)&&(Board[lastMove->toSquare])))){ //can move one or two squares forward or perform a capture
          return 1;
        }
        if(mov_dir==-16){ //If the pawn has moved two squares,
          game->enPassantSquare = mov_color==WHITE ? lastMove->toSquare+8:lastMove->toSquare-8; //update the en passant square. Right behind the pawn
        }
      }else{
        if(((mov_dir==-7)||(mov_dir==-9)) && (game->enPassantSquare==lastMove->toSquare)){ //Is the move an en passant?
          Uint8 captured_en_passant_square = mov_color==WHITE ? lastMove->toSquare+8:lastMove->toSquare-8;
          Board[captured_en_passant_square] = NONE; //Remove the captured piece from the board
          break;
        }
        if(!(mov_dir==-8||((mov_dir==-7)&&(Board[lastMove->toSquare]))||((mov_dir==-9)&&(Board[lastMove->toSquare])))){ //Else it can only move one square or perform a capture
          return 1;
          }
        }
      break;

    default: return 1;
    }

  //Check if the ally King is in check after the move. If it is, the move is ilegal. Undo last move
  //Check if the enemy King is in checkmate. If it is, finish the game
  return 0;
}
