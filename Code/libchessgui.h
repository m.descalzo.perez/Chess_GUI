
#ifndef LIBCHESSGUI_H
#define LIBCHESSGUI_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
//#include <SDL2/SDL_audio.h>
//#include <SDL2/SDL_timer.h>
//#include <SDL2/SDL_mouse.h>
//#include <SDL2/SDL_events.h>

//Dimensions of the screen
#define WINDOW_WIDTH 1280
#define WINDOW_HEIGHT 720

//These definitions are used to identify each piece on the board with a int
//Changing this numbers may produce bugs, due to the isMoveLegal() function implementation
#define WHITE 8
#define BLACK 16
#define BOTH_COLOURS (WHITE|BLACK)

#define NONE 0
#define PAWN 1
#define KNIGHT 2
#define BISHOP 3
#define ROOK 4
#define QUEEN 5
#define KING 6
#define ANY_PIECE 7
#define W_PAWN (WHITE|PAWN)
#define W_KNIGHT (WHITE|KNIGHT)
#define W_BISHOP (WHITE|BISHOP)
#define W_ROOK (WHITE|ROOK)
#define W_QUEEN (WHITE|QUEEN)
#define W_KING (WHITE|KING)
#define B_PAWN (BLACK|PAWN)
#define B_KNIGHT (BLACK|KNIGHT)
#define B_BISHOP (BLACK|BISHOP)
#define B_ROOK (BLACK|ROOK)
#define B_QUEEN (BLACK|QUEEN)
#define B_KING (BLACK|KING)

//Used to know what castling rights have each player
#define NO_CASTLE_RIGHTS 0
#define WHITE_CASTLE_KING 1
#define WHITE_CASTLE_QUEEN 2
#define BLACK_CASTLE_KING 4
#define BLACK_CASTLE_QUEEN 8
#define ALL_CASTLE_ALL 15

//Other definitions, to make the code easier to read
#define MOVE_SOUND 1
#define CAPTURE_SOUND 2
#define CHECK_MATE_SOUND 3

//Private struct containing details of a wav file. Used to produce sound
typedef struct Wav_file{
  SDL_AudioSpec wav_spec;
  Uint32 wav_length;
  Uint8 *wav_buffer;
  SDL_AudioDeviceID m_device;
}Wav_file;

//Last move played
typedef struct ChessMove{
  Uint8 fromSquare;
  Uint8 toSquare;
  Uint8 pieceMoved; //Current piece "attached" to the mouse
}ChessMove;

//Variables describing the state of the game, at the start of the game, defined by a FEN position
typedef struct ChessGame{
  Uint8 enPassantSquare; //en passant target square
  Uint8 nextToPlay; //White's turn or Black's
  Uint8 castleRights; //Who can castle and what side
  Uint8 halfMoveClock; //Used for the 50-move rule
  int fullMoveClock;
}ChessGame;

int initSDL_resources(SDL_Window** win, SDL_Renderer** rend, SDL_Surface** surface, SDL_Texture** tex);
void delSDL_resources(SDL_Window** win, SDL_Renderer** rend, SDL_Surface** surface, SDL_Texture** tex);

int initSDL_Sound(Wav_file* move_wav, Wav_file* capture_wav);
int ProduceSound(Wav_file* file_wav);
void delSDL_Soud(Wav_file* move_wav, Wav_file* capture_wav);

void ParseFenToBoard(char* fen, Uint8* Board, Uint8 fen_len, ChessGame* game);
Uint8 isMoveIlegal(Uint8* Board, ChessMove* lastMove, ChessGame* game);

void DrawBoard(SDL_Renderer* renderer, Uint16 squareSize, Uint16 xPos,  Uint16 yPos, SDL_Color *Dark, SDL_Color *Light, SDL_Color* TransformCol, ChessMove lastMove, Uint8 pointOfView);

SDL_Rect FindSpriteFromTex(Uint8 piece);
void DrawPieces(Uint8* Board, int squareSize, SDL_Renderer* rend, SDL_Texture* tex, Uint8 pointOfView);

#endif
